####KFlynn_DroneCode.md
#Drone Code documentation

The Automated drone, Project Frozen Fist, will follow a routine once activated. The routine is as follows:

1. Drone Activates
2. Find Target
 From here the drone will either calculate a firing solution until target is destroyed or return to searching for targets.
3. If no target is found, the drone will continue on its path to the objective point.
4. Drone will ocassionally check for target while it travels
5. Once objective is reached, Drone will deactivate
    
